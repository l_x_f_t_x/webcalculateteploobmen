﻿using CalculateLibrary1;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;
using WebCalculate.Data;
using WebCalculate.Models;

namespace WebCalculate.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;

        private readonly Context _context;

        public LoginController(ILogger<LoginController> logger, Context context)
        {
            _logger = logger;
            _context = context;
        }
        
        [HttpPost]
        public async Task<IActionResult> Index(string email, string password)
        {
            var user = _context.Users.FirstOrDefault(x => x.email == email && x.password == password);
            
            if(user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim("Id", user.id.ToString()),
                    new Claim(ClaimTypes.Name, user.email),
                    new Claim(ClaimTypes.Role, "user"),
                };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Cookies");
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> LogOut()
        {

            await HttpContext.SignOutAsync();
            return RedirectToAction("Index");
        }

    }
}