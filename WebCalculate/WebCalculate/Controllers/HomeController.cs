﻿using CalculateLibrary1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;
using WebCalculate.Data;
using WebCalculate.Models;

namespace WebCalculate.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly Context _context;

        private int _userId;

        public HomeController(ILogger<HomeController> logger, Context context)
        {
            _logger = logger;
            _context = context;
            
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            int.TryParse(User.FindFirst("Id")?.Value, out _userId);
        }
        
        [HttpPost]
        public IActionResult Result(CalculateInput input)
        {

            if (!string.IsNullOrEmpty(input.name))
            {
                var existhistory = _context.Histories.FirstOrDefault(x => x.name == input.name);

                if(existhistory != null)
                {
                    existhistory.Gm = input.Gm;
                    existhistory.Cm = input.Cm;
                    existhistory.Wr = input.Wr;
                    existhistory.S = input.S;
                    existhistory.V = input.V;
                    existhistory.d = input.d;
                    existhistory.Ho = input.Ho;
                    existhistory.Temp_1 = input.Temp_1;
                    existhistory.temp_2 = input.temp_2;

                    _context.Histories.Update(existhistory);
                    _context.SaveChanges();
                }
                else
                {
                    var history = new History
                    {
                        Gm = input.Gm,
                        Cm = input.Cm,
                        Wr = input.Wr,
                        S = input.S,
                        V = input.V,
                        d = input.d,
                        Ho = input.Ho,
                        Temp_1 = input.Temp_1,
                        temp_2 = input.temp_2,
                        name = input.name,
                        user_id = _userId
                    };

                    _context.Histories.Add(history);
                    _context.SaveChanges();
                }
             
            }

            
            var lib = new CalculateClass(input);
            var result_m = lib.Solve();
            if (ModelState.IsValid)
                return View(result_m);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Index(int? HistoryId)
        {
            var viewModel = new HomeViewModel();


            if(HistoryId != null)
            {
               viewModel.History = _context.Histories.
                    Where(x => x.user_id == _userId || x.user_id == 0).
                    FirstOrDefault(x => x.id == HistoryId);
            }



            viewModel.HistoryList = _context.Histories.
                Where(x => x.user_id == _userId || x.user_id == 0).
                ToList();

            return View(viewModel);
        }


        [HttpGet]
        [Authorize]
        public IActionResult Remove(int? HistoryId)
        {
            var History = _context.Histories.
                Where(x => x.user_id == _userId || x.user_id == 0).
                FirstOrDefault(x => x.id == HistoryId);
            
            if(History != null)
            {
                _context.Histories.Remove(History);
                _context.SaveChanges();

                TempData["message"] = $"Вариант {History.name} удален";
            }
            else
            {
                TempData["message"] = $"Вариант не найден";
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}