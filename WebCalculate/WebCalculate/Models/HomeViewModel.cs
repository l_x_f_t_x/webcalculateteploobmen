﻿using WebCalculate.Data;

namespace WebCalculate.Models
{
    public class HomeViewModel
    {
        public History? History { get; set; }
        public List<History>? HistoryList { get; set; }
    }
}
