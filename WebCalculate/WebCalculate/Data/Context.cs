﻿using Microsoft.EntityFrameworkCore;

namespace WebCalculate.Data
{
    public class Context : DbContext
    {
        public DbSet<Users> Users { get; set; }
        public DbSet<History> Histories { get; set; }
        public Context(DbContextOptions<Context> options) : base (options)
        {

        }
    }
}
