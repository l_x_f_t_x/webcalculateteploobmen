﻿using System.ComponentModel.DataAnnotations;

namespace WebCalculate.Data
{
    public class History
    {
        [Key]   
        public int id { get; set; }
        public int? user_id { get; set; }
        public string name { get; set; }
        public double Gm { get; set; }
        public double Cm { get; set; }
        public double Wr { get; set; }
        public double S { get; set; }
        public double V { get; set; }
        public double d { get; set; }
        public double Ho { get; set; }
        public double Temp_1 { get; set; }
        public double temp_2 { get; set; }
    }
}
