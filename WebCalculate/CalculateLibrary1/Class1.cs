﻿using System.Security.Cryptography;

namespace CalculateLibrary1
{
    public class CalculateClass
    {
        public double Gm { get; set; }
        public double Cm { get; set; }
        public double Wr { get; set; }
        public double S { get; set; }
        public double V { get; set; }
        public double d { get; set; }
        public int Ho { get; set; }
        public double Temp_1 { get; set; }
        public double temp_2 { get; set; }

        public CalculateClass(CalculateInput input)
        {
            Gm = input.Gm;
            Cm = input.Cm;
            Wr = input.Wr;
            S = input.S;
            V = input.V;
            d = input.d;
            Ho = input.Ho;
            Temp_1 = input.Temp_1;
            temp_2 = input.temp_2;
        }

        public CalculateOutput Solve()
        {
            double[] numbers = new double[Ho*2+1];
            double num = 0;
            for (int i = 0; i < Ho * 2 + 1; i++)
            {
                if (i == 0)
                    numbers[i] = num;
                else
                    numbers[i] = num;
                num = num + 0.5;
            }   

            var model = new CalculateOutput()
            {
                Mas = new List<CalculateOutputMas>()
            };

            foreach (var i in numbers)
            {
                var mas = new CalculateOutputMas
                {
                    h = i,
                    m = getm(),
                    Y_0 = getY_0(),
                    m_y = getm_y(),
                    Y = getY(i),
                    a2 = geta2(i),
                    a3 = geta3(i),
                    a4 = geta4(i),
                    a5 = geta5(i),
                    a6 = geta6(i),
                    a7 = geta7(i),
                    a8 = geta8(i),
                };
                model.Mas.Add(mas);
            }
            return model;
        }

        public double getm()
        {
            return (Gm * Cm) / (Wr * S * 4.15265);
        }
        private double getY_0()
        {
            return (V * Ho) / (S * Wr * 1000);
        }
        private double getm_y()
        {
            double m = getm();
            double Y_0 = getY_0();
            return (1 - m * Math.Exp((-(1 - m) * Y_0) / (m)));
        }

        private double getY(double i)
        {
            return (V * i) / (S * Wr * 1000);

        }
        private double geta2(double i)
        {
            double m = getm();
            double Y = getY(i);
            return (1 - Math.Exp(((m - 1) * Y) / (m)));
        }
        private double geta3(double i)
        {
            double m = getm();
            double Y = getY(i);
            return (1 - m * Math.Exp(((m - 1) * Y) / (m)));
        }
        private double geta4(double i)
        {
            double m = getm();
            double a2 = geta2(i);
            double Y_0 = getY_0();
            return ((a2) / (1 - m * Math.Exp(((m - 1) * Y_0) / (m))));
        }
        private double geta5(double i)
        {
            double m = getm();
            double a3 = geta3(i);
            double Y_0 = getY_0();
            return ((a3) / (1 - m * Math.Exp(((m - 1) * Y_0) / (m))));
        }
        private double geta6(double i)
        {
            double a4 = geta4(i);
            return (temp_2 + (Temp_1 - temp_2) * a4);
        }
        private double geta7(double i)
        {
            double a5 = geta5(i);
            return (temp_2 + (Temp_1 - temp_2) * a5);
        }
        private double geta8(double i)
        {
            double a6 = geta6(i);
            double a7 = geta7(i);
            return a6 - a7;
        }

    }
}