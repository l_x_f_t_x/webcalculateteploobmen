﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateLibrary1
{
    public class CalculateInput
    {
        public double Gm { get; set; }
        public double Cm { get; set; }
        public double Wr { get; set; }
        public double S { get; set; }
        public double V { get; set; }
        public double d { get; set; }
        public int Ho { get; set; }
        public double Temp_1 { get; set; }
        public double temp_2 { get; set; }
        public string name { get; set; }



    }
}
