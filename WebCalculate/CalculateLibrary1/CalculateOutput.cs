﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateLibrary1
{
    public class CalculateOutput
    {
        public List<CalculateOutputMas> Mas { get; set; }
    }

    public class CalculateOutputMas
    {
        public double h { get; set; }
        public double m { get; set; }

        public double Y_0 { get; set; }
        public double m_y { get; set; }
        public double Y { get; set; }
        public double a2 { get; set; }
        public double a3 { get; set; }

        public double a4 { get; set; }
        public double a5 { get; set; }
        public double a6 { get; set; }
        public double a7 { get; set; }
        public double a8 { get; set; }
    }

}
